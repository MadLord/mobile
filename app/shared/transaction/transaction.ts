export class Transaction {
  "_id": String;
  "titre_transaction": String;
  "date_transaction": Date;
  "paye": String;
  "id_payee": String;
  "id_payeur": String;
  "montant": number;
  "statut": String;
}