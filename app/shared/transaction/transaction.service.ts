import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import { SqliteService } from "../sqlite/sqlite.service";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { Transaction } from "./transaction";
import { Config } from "../config";

@Injectable()
export class TransactionService {
  constructor(private http: Http) { }


  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }

  getAllTransactions(): Observable<Transaction[]> {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("x-access-token", Config.token);


    return this.http.get(
      Config.apiUrl+'/api/factures',
      { headers: headers }
    )
      .map(response => response.json().factures)
      .catch(this.handleErrors);
  }

  payTransaction(id: String) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("x-access-token", Config.token);
    return this.http.post(
      Config.apiUrl+'/api/factures/payer/'+id,{},
      { headers: headers }
    )
      .map((response)=> response.json())
      .catch(this.handleErrors);
  }
}