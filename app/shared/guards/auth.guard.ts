import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Config } from "../config";

import { Observable } from "rxjs/Rx";
import { UserService } from "../user/user.service";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private userService: UserService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
      return this.userService.refreshToken().map((response: any) => {
        console.log("je passe");
        if (response.success) {
          Config.token = response.token;
          Config.user = response.user;
          return true;
        }
        else {
          Config.connected = false;
          console.log(response.message);
          this.router.navigate([""]);
          return false;
        }
    }) //*/

  }
}