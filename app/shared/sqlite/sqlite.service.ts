import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import { User } from "../user/user";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
var Sqlite = require("nativescript-sqlite");

@Injectable()
export class SqliteService {
  

  private createDB() {
    return new Promise((resolve, reject) => {
      return (new Sqlite("MyXchange")).then((db) => {
        db.execSQL("CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, prenom TEXT, solde INTEGER, token TEXT)")
          .then(id => {
            resolve(db);
          }, error => {
            alert("CREATE TABLE ERROR " + JSON.stringify(error.json()));
            reject(error);
          });
      }, error => {
        alert("OPEN DB ERROR" + JSON.stringify(error.json()));
      })
    });
  }

  public insert(user: User) {
    return new Promise((resolve, reject) => {
      this.createDB().then((res: any) => {
        res.execSQL("DELETE FROM user WHERE 1=1").then(() => {
          return res.execSQL("INSERT INTO user (nom, prenom, solde, token) VALUES (?, ?, ?, ?)",
            [user.nom, user.prenom, user.solde, user.token]).then(id => {
              //this.handleErrors("INSERT RESULT", id);
              resolve(true);
            }, error => {
              reject(error);
            });
        }, error => {
          reject(error);
        });
      })
    });
  }

  public selectAll() {
    return new Promise((resolve, reject) => {
      this.createDB().then((res: any) => {
        return res.all("SELECT * FROM user").then(users => {
          let user = new User();
          for (let row in users) {
            user["nom"] = String(users[0][1]),
              user["prenom"] = String(users[0][2]),
              user["solde"] = Number(users[0][3]),
              user["token"] = String(users[0][4])
          }
          resolve(user);

        }, error => {
          reject(error);
        });
      })
    })
  }

}