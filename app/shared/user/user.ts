export class User {
  "_id": String;
  "nom": String;
  "prenom": String;
  "mail": String;
  "tel": String;
  "mdp": String;
  "token": String;
  "solde": Number;
}