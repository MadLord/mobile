import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import { SqliteService } from "../sqlite/sqlite.service";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { User } from "./user";
import { Config } from "../config"; 

@Injectable()
export class UserService {
  constructor(private http: Http) { }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }

  login(user: User) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");

    return this.http.post(
      Config.apiUrl,
      JSON.stringify(user),
      { headers: headers }
    )
      .map(response => response.json())
      .do(data => {
        if(data.success)
          Config.token = data.token;
      })
      .catch(this.handleErrors);
  }

  logout() {
    
  }

  refreshToken() {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("x-access-token", Config.token);

     return this.http.get(
        Config.apiUrl+'/api/refresh',
        { headers: headers }
      )
      .map(response => response.json())

  }
}