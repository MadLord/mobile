import { Component, OnInit, Input } from "@angular/core";
import { Transaction } from "../../../shared/transaction/transaction";


@Component({
  selector: "item-selector",
  templateUrl: "pages/list/item/item.html",
  styleUrls: ["pages/list/item/item-common.css", "pages/item/item.css"]
})
export class ItemComponent implements OnInit {

  @Input() transaction: Transaction;

  constructor(
  ) {
  }

  ngOnInit() {
  } 
}