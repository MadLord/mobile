import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../../shared/user/user.service";
import { TransactionService } from "../../shared/transaction/transaction.service";
import { Transaction } from "../../shared/transaction/transaction";
import { User } from "../../shared/user/user";
import * as listViewModule from "tns-core-modules/ui/list-view";
import { BarcodeScanner } from 'nativescript-barcodescanner';
import { Config } from "../../shared/config";


@Component({
  selector: "list",
  templateUrl: "pages/list/list.html",
  styleUrls: ["pages/list/list-common.css", "pages/list/list.css"]
})
export class ListComponent implements OnInit {

  public transactions: Transaction[];
  public user: User;
  constructor(
    private router: Router,
    private userService: UserService,
    private transactionService: TransactionService,
    private barcodeScanner: BarcodeScanner
  ) {
  }

  ngOnInit() {
    this.initTransaction();
    this.refreshUser();

  }

  initTransaction() {
    this.transactionService.getAllTransactions().subscribe((transactions: Transaction[]) => {
      transactions.forEach((element: Transaction) => {
        element;
        switch (Config.user._id) {
          case element.id_payee:
            if (element.paye) {
              element.statut = "crédité";
            }
            else {
              element.statut = "impayé";
            };
            break;
          case element.id_payeur:
            if (element.paye) {
              element.statut = "débité";
            }
            else {
              element.statut = "à payer";
            };
            break;
          default:
            element.statut = "";
        }
      })
      this.transactions = transactions;
    });
  }

  refreshUser(){
    return this.userService.refreshToken().map((response: any) => {
      console.log("je passe");
      if (response.success) {
        Config.token = response.token;
        Config.user = response.user;
        this.user = Config.user;
        return true;
      }
      else {
        Config.connected = false;
        console.log(response.message);
        this.router.navigate([""]);
        return false;
      }
  }).subscribe(()=>{

  }); //*/
  }


  payer() {
    this.barcodeScanner.scan({
      formats: "QR_CODE, EAN_13",
      showFlipCameraButton: true,
      preferFrontCamera: false,
      showTorchButton: true,
      beepOnScan: true,
      torchOn: false,
      resultDisplayDuration: 500,
      orientation: "orientation",
      openSettingsIfPermissionWasPreviouslyDenied: true //ios only 
    }).then((result) => {
      console.log("je passe le QR code");
      
      if (result.format !== "QR_CODE") {
        console.log("Ce QR Code n'est pas adapté à l'application")
        alert("Ce QR Code n'est pas adapté à l'application")
      }
      else {
        this.transactionService.payTransaction(result.text).subscribe(data => {
          console.log("data : " + JSON.stringify(data));

          if (data.success) {
            if(data.facture){
              console.log("Le paiement de la facture '" + data.facture.titre_transaction +
              "' s'est déroulé avec succès");
            alert("Le paiement de la facture '" + data.facture.titre_transaction +
              "' s'est déroulé avec succès");
              this.initTransaction();
              this.refreshUser();
            }
            
          }
          else if (data.message !== "montant insufisant.") {
            console.log("data :" + data);
            console.log("la facture n'a pas été retrouvé, avez-vous scanné le bon QRCode");
            alert("la facture n'a pas été retrouvé, avez-vous scanné le bon QRCode");
          }

          else {
            console.log("votre solde est insuffisant");
            alert("votre solde est insuffisant");
          }

        }, error => {
          console.log("Ce QR Code n'est pas adapté à l'application")
          alert("Ce QR Code n'est pas adapté à l'application")
        })
      }
    }, (errorMessage) => {
      console.log(errorMessage);
      console.log("Votre appareil semble ne pas prendre en compte la lecture de QR Code.");
      alert("Votre appareil semble ne pas prendre en compte la lecture de QR Code.");
    }
      );
  }
}