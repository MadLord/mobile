import { Component, OnInit } from "@angular/core";
import { Page } from "ui/page";
import { Router } from "@angular/router";
import { UserService } from "../../shared/user/user.service";
import { User } from "../../shared/user/user";
import { SqliteService } from "../../shared/sqlite/sqlite.service";
import { Config } from "../../shared/config";


@Component({
  selector: "login",
  providers: [UserService, SqliteService],
  templateUrl: "pages/login/login.html",
  styleUrls: ["pages/login/login-common.css", "pages/login/login.css"],
})
export class LoginComponent implements OnInit {
  user: User;
  isLoggingIn = true;

  constructor(private router: Router,
    private userService: UserService,
    private sqliteService: SqliteService,
    private page: Page) {
    this.user = new User();
    this.user.mail = "john@doe.com";
    this.user.mdp = "String";

  }
  login() {
    this.userService.login(this.user)
      .subscribe(
      (data) => {
        if (data.success)
        {
          this.sqliteService.insert(this.user).then(user => {
            Config.token = data.token;
            this.router.navigate(["/list"]);
            Config.user = data.user;
          },
            error => {
              alert("An error occured");
            });
        }
          
        else {
          alert("Combinaison mail / mot de passe invalide");
        }
      },
      (error) => alert("Serveur injoignable, veuillez vous assurer que votre appareil est connecté à internet.")
      );
  }
  ngOnInit() {
    this.page.actionBarHidden = true;
    //this.page.backgroundImage = "res://home_bg";
    this.sqliteService.selectAll().then((user: User) => {
      if (user != null && user.token && Config.connected) {
        this.user = user;
        Config.token = user.token.toString();
        this.router.navigate(["/list"]);
      }
    });//*/
  }
}