import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { BarcodeScanner } from 'nativescript-barcodescanner';

@Component({
  selector: "scanner",
  templateUrl: "pages/scanner/scanner.html",
  styleUrls: ["pages/scanner/scanner-common.css", "pages/scanner/scanner.css"]
})
export class ScannerComponent {
  constructor(private barcodeScanner: BarcodeScanner) {
  }

  public onScan() {
    
  }


}