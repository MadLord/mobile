import { NgModule } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { BarcodeScanner } from 'nativescript-barcodescanner';

import { SqliteService } from "./shared/sqlite/sqlite.service";
import { UserService } from "./shared/user/user.service";
import { TransactionService } from "./shared/transaction/transaction.service";
import { AuthGuard } from "./shared/guards/auth.guard";

import { AppComponent } from "./app.component";
import { routes, navigatableComponents } from "./app.routing";

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes)
  ],
  declarations: [
    AppComponent,
    ...navigatableComponents
  ],
  bootstrap: [AppComponent],
  providers: [BarcodeScanner, SqliteService, AuthGuard, UserService, TransactionService]
})
export class AppModule {}