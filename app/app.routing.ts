import { LoginComponent } from "./pages/login/login.component";
import { ListComponent } from "./pages/list/list.component";
import { ScannerComponent } from "./pages/scanner/scanner.component";
import { AuthGuard } from "./shared/guards/auth.guard";

export const routes = [
  { path: "", component: LoginComponent },
  { path: "list", component: ListComponent, canActivate: [AuthGuard] },
  { path: "scanner", component: ScannerComponent },
];

export const navigatableComponents = [
  LoginComponent,
  ListComponent,
  ScannerComponent
];
